
List of my personal projects wherein I personaly did it from system analysis, coding and up to deployment in Amazon AWS EC2.

1. CalmAffection
     - Multi level network marketing system.
     - Includes web portals (Admin and Member Account) and Backend
     - This is a binary system and level up system model
     - Includes SOA generation, Reportings and Analysis

2. Librascrop
     - Raffle entry and winning system
     - Includes web portals (Admin and Agent)
     - Has Reportings and data analysis

3. GiftBeam
     - Mobile application (Android) for reward system

4. Simple Profiling
     - Simple viewing and editing of profile.

